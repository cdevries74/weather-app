# WEATHER APP
# Written Fall 2019 -- Chris DeVries
# Hosting Infra Academy

import json
import traceback
import logging
import requests
import geopy.geocoders
from geopy.geocoders import Nominatim

# FUNCTION
# Prompts the user for city ST (state) input
# If input accepted, calls API, uses GeoPy to get lat & long from city state name
# Then assigns my_loc and exits function
# If input not accepted, prompts user and recursively calls to return to the beginning of the function

def get_lat_lon_from_city_state():
    user_city = input("Enter City & State: ")

    if user_city == "":
        my_loc = None
    else:
        try:
            city, state = user_city.split()
            my_loc_url = "https://fu1hgw3pmd.execute-api.us-east-1.amazonaws.com/v1/get-started/" + "?city=" + city + "&" + "state=" + state
            data = requests.get(my_loc_url)
            data = data.json()
            my_loc = (data)
            return my_loc   
        except:
            print("Provide city & state")
            my_loc = get_lat_lon_from_city_state()
    
    return my_loc

# FUNCTION
# If no city state is provided, the application finds the public IP address
# of the computer where the application is run from, and returns that IP address

def get_pub_ip():
    ip_url = "https://ipvigilante.com"
    data = requests.get(ip_url)
    
    response_dict = data.json()

    my_ip = response_dict["data"]["ipv4"]

    return my_ip

# FUNCTION
# Once public IP address is obtained, queries API to get lat and lon of the location

def get_location(my_ip):
    loc_url = "https://fu1hgw3pmd.execute-api.us-east-1.amazonaws.com/v1/get-location/" + "?ip=" + my_ip
    data = requests.get(loc_url)
    data = data.json()
    location = (data["latitude"],data["longitude"])

    return (location)

# FUNCTION
# Once lat and lon are obtained, queries API to get weather conditions and forecast

def get_forecast(lat,lon):
    forecast_url = "https://fu1hgw3pmd.execute-api.us-east-1.amazonaws.com/v1/get-weather/" + "?latitude=" + lat + "&" + "longitude=" + lon
    data = requests.get(forecast_url)
    data = data.json()
    
    return data

my_loc = get_lat_lon_from_city_state()
if my_loc == None:
    my_ip = get_pub_ip()
    my_loc = get_location(my_ip)
lat,lon = my_loc
forecast = get_forecast(lat,lon)
print(forecast)