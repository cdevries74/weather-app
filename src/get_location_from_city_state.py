import json
import requests
import logging
import geopy.geocoders
from geopy.geocoders import Nominatim

log = logging.getLogger()
log.setLevel(logging.INFO)

def lambda_handler(event, context):
    try:
        queryparams = event['queryStringParameters']
        
        city = queryparams['city']
        state = queryparams['state']
        
        user_city = city + " " + state
        
        if user_city == "":
            location = None
        else:
            geolocator = Nominatim(user_agent='myapplication')
            location = geolocator.geocode(f"{user_city}")
            location = (location.raw['lat'],location.raw['lon'])      
        
        return {
            'statusCode': 200,
            'body': json.dumps(location)
        }
    except:
        log.error(traceback.format_exc())
        return {
            "statusCode": 500,
            "body": "Error executing lambda"
        }