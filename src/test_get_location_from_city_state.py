# UNIT TEST
# Tests API to obtain location based on user input

import unittest
from get_location_from_city_state import lambda_handler
from geopy.geocoders import *

class TestLocationLookup(unittest.TestCase):

    def test_get_coordinates(self):
        mock_api_call = {
            'queryStringParameters': {
                'city': 'dover',
                'state': 'nh'
            }
        }
        
        res = lambda_handler(mock_api_call, {})
        self.assertEqual(res['statusCode'], 200)

if __name__ == '__main__':
    unittest.main()