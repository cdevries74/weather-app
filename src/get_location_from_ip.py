import json
import traceback
import logging
import requests

log = logging.getLogger()
log.setLevel(logging.INFO)

def lambda_handler(event, context):
    try:
        query_params = event['queryStringParameters']
        if not 'ip' in query_params:
            return {
                "statusCode": 400,
                "body": "Missing 'ip' query parameter"
            }

        ip_addr = query_params['ip']
        api_url = f"https://ipvigilante.com/{ip_addr}"

        data = requests.get(api_url)
        if data.status_code == 400:
            return {
                "statusCode": 400,
                "body": "Invalid Request to ipvigilante: %s" % data.text
            }
        elif data.status_code == 500:
            return {
                "statusCode": 500,
                "body": "Error from IP Vigilante: %s" % data.text
            }
        response_dict = data.json()
        location = {
            "latitude": response_dict["data"]["latitude"],
            "longitude": response_dict["data"]["longitude"],
        }
        return {
            "body": json.dumps(location),
            "statusCode": 200
        }
    except e:
        log.error(traceback.format_exc())
        return {
            "statusCode": 500,
            "body": "Error executing lambda"
        }